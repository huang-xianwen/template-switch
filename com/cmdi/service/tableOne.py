from openpyxl.reader.excel import load_workbook

def compute_data(workbook, read_workbook):
    sheet_names = read_workbook.sheetnames
    if '表一' not in sheet_names:
        raise ValueError('表一不存在或命名错误!')
    data_map = read_data(read_workbook)
    write_data(workbook, data_map)


def read_data(read_workbook):
    # 获取指定的Sheet页
    sheet = read_workbook.get_sheet_by_name('表一')
    data_map = {}
    start_row = 0
    read_length = 18
    for idx, val in enumerate(sheet.iter_rows(min_row=sheet.min_row, max_col=1, values_only=True), start=1):
        if val[0] == 'Ⅰ':
            start_row = idx + 1
            break
    for row in sheet.iter_rows(min_row=start_row, max_row=start_row + read_length):
        key = row[2].value
        if type(key)==str:
            key = key.strip()
        value = (row[3].value, row[4].value, row[5].value, row[6].value, row[7].value,
                 row[8].value, row[9].value, row[10].value, row[11].value, row[12].value)
        data_map[key] = value
    return data_map


def write_data(workbook, data_map):
    sheet = workbook.get_sheet_by_name('表一')
    for row in sheet.iter_rows(min_row=4, max_row=11):
        key = row[2].value
        if type(key)==str:
            key = key.strip()
        value = data_map.get(key)
        index = 0
        if value is None or len(value) == 0:
            continue
        for cell in row:
            if index < 4:
                index += 1
                continue
            cell_val = value[index - 4]
            if cell_val == '' or cell_val is None:
                index += 1
                continue
            if type(cell_val) == float:
                cell_val = round(cell_val, 2)
            cell.value = cell_val
            index += 1


# if __name__ == '__main__':
#     wpath = 'F:\\pycharm\\template-switch\\com\\cmdi\\assets\\PMS概预算导入模板.xlsx'
#     rpath = 'C:\\Users\\Administrator\\Desktop\\模板\\传输预算模板（标准字段）-传输设备.xlsx'
#     workbook = load_workbook(filename=wpath)
#     rb = load_workbook(filename=rpath, data_only=True)
#     compute_data(workbook, rb)