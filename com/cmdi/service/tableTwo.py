from openpyxl.reader.excel import load_workbook

def compute_data(workbook, read_workbook):
    sheet_name = ('表二', '表二（折扣前）')
    sheet_names = read_workbook.sheetnames
    for name in sheet_name:
        if name not in sheet_names:
            print('警告：未找到'+name+'，如果模板不需要请忽略！')
            continue
        data_map = read_data(read_workbook, name)
        write_data(workbook, name, data_map)


def read_data(read_workbook, name):
    # 获取指定的Sheet页
    sheet = read_workbook.get_sheet_by_name(name)
    data_map = {}
    start_row = 0
    read_length = 20
    for idx, val in enumerate(sheet.iter_rows(min_row=sheet.min_row, max_col=1, values_only=True), start=1):
        if val[0] == 'Ⅰ':
            start_row = idx + 1
            break
    for row in sheet.iter_rows(min_row=start_row, max_row=start_row + read_length):
        key = row[1].value
        if type(key) == str:
            key = key.strip()
        if key is not None:
            value = row[3].value
            data_map[key] = value
        if row[6].value is None:
            continue
        key2 = row[6].value
        if type(key2) == str:
            key2 = key2.strip()
        if key2 is not None:
            value2 = row[8].value
            data_map[key2] = value2
    return data_map


def write_data(workbook, sheet_name, data_map):
    sheet = workbook.get_sheet_by_name(sheet_name)
    max_row = 40 if sheet_name == '表二' else 44
    for row in sheet.iter_rows(min_row=3, max_row=max_row):
        key = row[1].value
        if type(key) == str:
            key = key.strip()
        if key is None:
            continue
        value = data_map.get(key)
        if type(value) == float:
            value = round(value, 2)
        row[4].value = value


# if __name__ == '__main__':
#     wpath = 'F:\\pycharm\\template-switch\\com\\cmdi\\assets\\PMS概预算导入模板.xlsx'
#     rpath = 'C:\\Users\\Administrator\\Desktop\\模板\\传输预算模板（标准字段）-传输设备.xlsx'
#     workbook = load_workbook(filename=wpath)
#     rb = load_workbook(filename=rpath, data_only=True)
#     compute_data(workbook, rb)
