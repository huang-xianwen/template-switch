from openpyxl.reader.excel import load_workbook

def compute_data(workbook, read_workbook):
    sheet_names = read_workbook.sheetnames
    if '表五甲' not in sheet_names:
        raise ValueError('表五甲不存在或命名错误!')
    data_map = read_data(read_workbook)
    write_data(workbook, data_map)


def read_data(read_workbook):
    # 获取指定的Sheet页
    sheet = read_workbook.get_sheet_by_name('表五甲')
    name_fee_map = {}
    start_row = 0
    read_length = 18
    for idx, val in enumerate(sheet.iter_rows(min_row=sheet.min_row, max_col=1, values_only=True), start=1):
        if val[0] == 'Ⅰ':
            start_row = idx + 1
            break
    for row in sheet.iter_rows(min_row=start_row, max_row=start_row + read_length):
        key = row[1].value
        if key == None:
            key = ''
        if type(key) == str:
            key = key.strip()
        name_fee_map[key + 'Ⅲ'] = row[2].value
        name_fee_map[key + 'Ⅳ'] = row[3].value
        name_fee_map[key + 'Ⅴ'] = row[4].value
        name_fee_map[key + 'Ⅵ'] = row[5].value
    return name_fee_map


def write_data(workbook, name_fee_map):
    sheet = workbook.get_sheet_by_name('表五甲')
    for row in sheet.iter_rows(min_row=4, max_row=23):
        key = row[1].value
        if key == None:
            key = ''
        if type(key)==str:
            key = key.strip()
        value0 = name_fee_map.get(key + 'Ⅲ')
        row[2].value = value0
        value1 = name_fee_map.get(key + 'Ⅳ')
        if type(value1) == float:
            value1 = round(value1, 2)
        row[4].value = value1
        value2 = name_fee_map.get(key + 'Ⅴ')
        if type(value2) == float:
           value2 = round(value2, 2)
        row[5].value = value2
        value3 = name_fee_map.get(key + 'Ⅵ')
        if type(value3) == float:
            value3 = round(value3, 2)
        row[6].value = value3

# if __name__ == '__main__':
#     wpath = 'F:\\pycharm\\template-switch\\com\\cmdi\\assets\\PMS概预算导入模板.xlsx'
#     rpath = 'C:\\Users\\Administrator\\Desktop\\模板\\传输预算模板（标准字段）-传输设备.xlsx'
#     workbook = load_workbook(filename=wpath)
#     rb = load_workbook(filename=rpath, data_only=True)
#     compute_data(workbook, rb)
