from openpyxl.reader.excel import load_workbook


def compute_data(workbook, read_workbook):
    sheet_names = read_workbook.sheetnames
    if '规则表' not in sheet_names:
        print('警告：未找到规则表，请生成模板后手动添加规则表折扣数据！')
        return
    data_map = read_data(read_workbook)
    write_data(workbook, data_map)


def read_data(read_workbook):
    # 获取指定的Sheet页
    sheet = read_workbook.get_sheet_by_name('规则表')
    data_map = {}
    for row in sheet.iter_rows(min_row=26, max_row=28):
        key = row[1].value
        if type(key)==str:
            key = key.strip()
        value = "{:.2%}".format(row[2].value)
        data_map[key] = value
    return data_map


def write_data(workbook, data_map):
    sheet = workbook.get_sheet_by_name('规则表')
    for row in sheet.iter_rows(min_row=26, max_row=28):
        key = row[1].value
        if type(key)==str:
            key = key.strip()
        value = data_map.get(key)
        if value is None or str(value) == '':
            print('警告:(规则表('+key+')对应折扣为空，请生成后手动输入!')
            continue
        row[2].value = value


# if __name__ == '__main__':
#     wpath = 'F:\\pycharm\\template-switch\\com\\cmdi\\assets\\PMS概预算导入模板.xlsx'
#     rpath = 'C:\\Users\\Administrator\\Desktop\\模板\\传输预算模板（标准字段）-传输设备.xlsx'
#     workbook = load_workbook(filename=wpath)
#     rb = load_workbook(filename=rpath, data_only=True)
#     compute_data(workbook, rb)