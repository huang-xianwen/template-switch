from openpyxl import load_workbook
import os


def write_file(workbook, read_path):
    root_path = os.path.dirname(read_path)
    root_path = os.path.dirname(root_path)
    dir_path = os.path.join(root_path, 'PMS转化结果')
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    filename = os.path.basename(read_path)
    names = filename.split('.')
    file_name = names[0] + '-' + 'PMS导入.' + names[1]
    workbook.save(os.path.join(dir_path, file_name))
    workbook.close()


def read_file(file_path):
    workbook = load_workbook(filename=file_path)
    return workbook

# if __name__ == '__main__':
#     wpath = 'F:\\pycharm\\template-switch\\com\\cmdi\\assets\\PMS概预算导入模板.xlsx'
#     rpath = 'C:\\Users\\Administrator\\Desktop\\传输预算模板（标准字段）-通信管道.xlsx'
#     workbook = load_workbook(filename=wpath)
#     rb = load_workbook(filename=rpath, data_only=True)
#     compute_data(workbook, rb)