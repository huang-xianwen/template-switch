from openpyxl.reader.excel import load_workbook

def compute_data(workbook, read_workbook):
    sheet_name = ('表三丙', '表三甲', '表三乙')
    sheet_names = read_workbook.sheetnames
    for name in sheet_name:
        if name not in sheet_names:
            print('警告：未找到' + name + '，如果模板不需要请忽略！')
            continue
        name_fee_list = read_data(read_workbook, name)
        write_data(workbook, name, name_fee_list)


def read_data(read_workbook, name):
    # 获取指定的Sheet页
    sheet = read_workbook.get_sheet_by_name(name)
    name_fee_list = []
    start_row = 0
    read_length = sheet.max_row
    for idx, val in enumerate(sheet.iter_rows(min_row=sheet.min_row, max_col=1, values_only=True), start=1):
        if val[0] == 'Ⅰ':
            start_row = idx + 1
            break
    value_len = 9 if name == "表三甲" else 10
    for row in sheet.iter_rows(min_row=start_row, max_row=start_row + read_length):
        if (type(row[4].value) == float or type(row[4].value) == int) and float(row[4].value) == 0:
            continue
        tempobj = []
        for idx in range(value_len):
            if value_len == 9:
                if idx == 7 or idx == 8:
                    if row[idx].value !=None and float(row[idx].value) == 0:
                            tempobj.append('')
                    else:
                            tempobj.append(row[idx].value)
                else:
                        tempobj.append(row[idx].value)
            if value_len == 10:
                if idx == 9:
                    if row[idx].value != None and float(row[idx].value) == 0:
                        tempobj.append('')
                    else:
                        tempobj.append(row[idx].value)
                else:
                    tempobj.append(row[idx].value)
        name_fee_list.append(tempobj)
    return name_fee_list


def write_data(workbook, name, name_fee_list):
    sheet = workbook.get_sheet_by_name(name)
    row_index = 0
    value_len = 9 if name == "表三甲" else 10
    for row in sheet.iter_rows(min_row=4, max_row=len(name_fee_list) + 3):
        for idx in range(value_len):
            value = name_fee_list[row_index][idx]
            if type(value) == float:
                value = round(value, 2)
            row[idx].value = value
        row_index = row_index + 1
#
# if __name__ == '__main__':
#     wpath = 'F:\\pycharm\\template-switch\\com\\cmdi\\assets\\PMS概预算导入模板.xlsx'
#     rpath = 'C:\\Users\\Administrator\\Desktop\\模板\\传输预算模板（标准字段）-传输设备.xlsx'
#     workbook = load_workbook(filename=wpath)
#     rb = load_workbook(filename=rpath, data_only=True)
#     compute_data(workbook, rb)