import datetime
import os
import queue
import sys
import time
import tkinter as tk
from tkinter import filedialog, ttk
from io import StringIO
from com.cmdi.main import main


window = tk.Tk()
log_queue = queue.Queue()

source_file_folder_var = tk.StringVar()
file_or_folder_var = tk.StringVar()

# 赋默认值
source_file_folder_var.set(os.getcwd())
file_or_folder_var.set("file")


# 创建一个StringIO对象来临时存储print输出
class PrintBuffer(StringIO):
    def __init__(self, target_queue):
        super().__init__()
        self.buffer_queue = target_queue

    def write(self, s):
        # 收集print信息并将其放入队列
        self.buffer_queue.put(s)
        return super().write(s)


sys.stdout = PrintBuffer(log_queue)



def formatPath(path):
    return os.path.normpath(path)


def insertLog(log):
    log_queue.put(log)


def start_switch():
    # 涉及matplotlib ，只能在主线程中使用，无法新开子线程
    switch()


# 定义一个装饰器，用于测量函数执行时间
def log_wrapper(function):
    def wrapper(*args, **kwargs):
        final_valid_date = datetime.datetime(2026, 5, 31, 0, 0)
        if datetime.datetime.now() > final_valid_date:
            insertLog("============已过期================\n")
            insertLog("该软件已过期，请联系CMDI-GX提供新版.\n")
            insertLog("============已过期================\n")
            return
        start = time.perf_counter()
        # insertLog("=======开始执行==========\n")

        try:
            result = function(*args, **kwargs)

            end = time.perf_counter()
            # insertLog("========执行完成============\n")
            # insertLog(f"{function.__name__} 执行时间: {end - start} 秒")
            return result

        except Exception as e:
            insertLog("执行出现异常:\n" + e.__class__.__name__ + ":" + str(e) + "\n")
            raise e

    return wrapper


@log_wrapper
def switch():

    path_list = source_file_folder_var.get()
    paths = path_list.rstrip(';').split(';')
    for index, path in enumerate(paths):
        dir_path = os.path.dirname(formatPath(path))
        dir_path = os.path.dirname(formatPath(dir_path))
        dir_path = os.path.join(dir_path, 'PMS转化结果')
        file_name = os.path.basename(formatPath(path))
        print('========开始转化========')
        print('正在处理文件'+str(index+1)+'：'+file_name)
        main(formatPath(path))
        print('========转化结束========')
    print('PMS转化结果将保存在目录：' + dir_path)


class GUI():
    def __init__(self, window, windowHeight=500, windowWidth=500):

        self.window = window
        self.windowHeight = windowHeight
        self.windowWidth = windowWidth

        # 数据有交互的变量
        self.logListText = None
        self.fromFolderEntry = None
        self.source_file_entry = None

        self.initGui()

        # 启动 after 方法
        self.window.after(100, self.showLog)
        # 进入消息循环
        self.window.mainloop()

    def initGui(self):
        # window
        self.window.title("PMS模板转化工具—— CMDI-GX")
        # curr_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        # icon_path = os.path.join(curr_dir, 'assets', 'icon.ico')
        # self.window.iconbitmap(icon_path)
        window_width = self.windowWidth
        window_height = self.windowHeight
        screen_width = self.window.winfo_screenwidth()
        screen_height = self.window.winfo_screenheight()

        window.geometry('%dx%d+%d+%d' % (
            window_width, window_height, (screen_width - window_width) / 2, (screen_height - window_height) / 2))

        frame = tk.Frame(window)
        frame.pack(fill=tk.BOTH, expand=tk.YES, padx=10, pady=10)

        # frame
        info_label_frame = ttk.LabelFrame(frame, text="基本信息")
        folder_label_frame = ttk.LabelFrame(frame, text="数据源配置")
        config_frame = tk.Frame(frame)
        start_frame = tk.Frame(frame)
        log_list_frame = tk.Frame(frame)

        info_label_frame.pack(fill=tk.X, expand=tk.YES, pady=4)
        folder_label_frame.pack(fill=tk.X, expand=tk.YES, pady=4, ipady=4)
        config_frame.pack(fill=tk.X)
        start_frame.pack(fill=tk.X, expand=tk.YES, pady=4)
        log_list_frame.pack(fill=tk.BOTH, expand=tk.YES, pady=4)

        # infoLableFrame
        fn_frame = tk.Frame(info_label_frame)
        author_frame = tk.Frame(info_label_frame)

        fn_frame.pack(fill=tk.X, expand=tk.YES, padx=2)
        author_frame.pack(fill=tk.X, expand=tk.YES, padx=2)

        # fnFrame
        tk.Label(fn_frame, text="本工具用于模板转化").grid(sticky=tk.E)
        tk.Label(fn_frame, text="1：格式请使用.xlsx").grid(sticky=tk.W)
        tk.Label(fn_frame, text="2：子表名称要对应上").grid(sticky=tk.W)
        tk.Label(fn_frame, text="3：不要有空列").grid(sticky=tk.W)
        tk.Label(fn_frame, text="4：表一、二、五属性名称强制要求与模板相同").grid(sticky=tk.W)
        # authorFrame

        author_label = tk.Label(author_frame, text="CMDI-GX")

        author_label.pack(side=tk.RIGHT)

        source_file_frame = tk.Frame(folder_label_frame)

        source_file_frame.pack(fill=tk.X, padx=3)

        template_file_frame  = tk.Frame(folder_label_frame)
        template_file_frame.pack(fill=tk.X, padx=3)
        # source file frame
        source_file_label = tk.Label(source_file_frame, text="数据文件")
        source_file_entry = ttk.Entry(source_file_frame, textvariable=source_file_folder_var)
        self.source_file_entry = source_file_entry
        file_radio = ttk.Radiobutton(source_file_frame, text="文件", variable=file_or_folder_var, value="file",)
        folder_radio = ttk.Radiobutton(source_file_frame, text="文件夹", variable=file_or_folder_var, value="folder",)
        source_file_button = ttk.Button(source_file_frame, text="选择", command=self.choose_file_or_folder)


        source_file_label.pack(side=tk.LEFT)
        source_file_entry.pack(side=tk.LEFT, fill=tk.X, expand=tk.YES, padx=6)
        file_radio.pack(side=tk.LEFT)
        folder_radio.pack(side=tk.LEFT)
        source_file_button.pack(side=tk.LEFT)

        output_frame = tk.LabelFrame(config_frame, text="")
        output_frame.pack(side=tk.RIGHT, padx=2)
        # startFrame
        startButton = ttk.Button(output_frame, text='开始转化', command=start_switch)
        startButton.pack(side=tk.LEFT, fill=tk.X, expand=tk.YES, ipady=1.5)

        # logListFrame
        scrollBar = tk.Scrollbar(log_list_frame)
        logListText = tk.Text(log_list_frame, height=100, yscrollcommand=scrollBar.set)
        self.logListText = logListText
        scrollBar.config(command=logListText.yview)

        scrollBar.pack(side=tk.RIGHT, fill=tk.Y)
        logListText.pack(side=tk.LEFT, fill=tk.BOTH, expand=tk.YES)

    def showLog(self):
        while not log_queue.empty():
            content = log_queue.get()
            self.logListText.insert(tk.END, content + "\n")
            self.logListText.yview_moveto(1)

        self.window.after(100, self.showLog)

    def set_source_folder_path(self):
        folder_path = tk.filedialog.askdirectory(initialdir=os.getcwd(), title="Select file")
        if len(folder_path) > 0:
            self.source_file_entry.delete(0, tk.END)
        path_list = ''
        for root, dirs, files in os.walk(folder_path):
            for file in files:
                path_list += formatPath(os.path.join(root, file)) + ';'
        self.source_file_entry.insert(0, path_list)
    def set_source_file_path(self):
        file_paths = tk.filedialog.askopenfilenames(initialdir=os.getcwd(), title="Select file",
                                            filetypes=[("excels 文件", ".xlsx"), ("所有文件", "*")])
        if len(file_paths) > 0:
            self.source_file_entry.delete(0, tk.END)
        path_list = ''
        for file_path in file_paths:
            if len(file_path) > 0:
                path_list += formatPath(file_path) + ';'
        if len(path_list) > 0:
            self.source_file_entry.insert(0, path_list)
    def choose_file_or_folder(self):
       if file_or_folder_var.get() == 'file':
           self.set_source_file_path()
       else:
           self.set_source_folder_path()

    # def radio_select(self):
        # print(file_or_folder_var.get())

if __name__ == "__main__":
    GUI(window)
