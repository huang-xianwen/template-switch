import os.path

from com.cmdi.service import tableOne, tableTwo, tableThree, tableFour, tableFive, tableRule
from com.cmdi.service.util.fileUtils import load_workbook, write_file


def main(read_path):
    curr_dir = os.path.dirname(os.path.abspath(__file__))
    write_path = os.path.join(curr_dir, 'assets', 'PMS概预算导入模板.xlsx')
    wb = load_workbook(write_path)
    rb = load_workbook(filename=read_path, data_only=True)
    tableOne.compute_data(wb, rb)
    tableTwo.compute_data(wb, rb)
    tableThree.compute_data(wb, rb)
    tableFour.compute_data(wb, rb)
    tableFive.compute_data(wb, rb)
    tableRule.compute_data(wb, rb)
    write_file(wb, read_path)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    read_path = 'C:\\Users\\Administrator\\Desktop\\模板\\传输预算模板（标准字段）-传输设备.xlsx'
    main(read_path)


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
